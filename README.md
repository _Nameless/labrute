
Currently on the repository : a very basic prototype made by Scorpio, modified by Nadawoo.

## Roadmap
### Gathering the data
Listing the variable/textual elements in JSON files :
- Listing the weapons : partially done (in the file /config/weapons.json)
- Listing the skills : 0% done
- Listing the characteristics (health, accuracy...) : 0% done
- Listing the pets : 0% done
- Listing the competences :
   - Talents (manual) : 0% done
   - Supers (random) : 0% done
   - Specialities (passive) : 0% done

### Coding
- Implement the weapons : done ✓
- Implement the skills : 0% done
- Implement the visible characteristics :
   - Health : done ✓
   - Strength : partially done
   - Agility : TODO
   - Speed : TODO
- Implement the hidden characteristics :
   - Endurance : TODO
   - Armure : TODO
   - Initiative : TODO
   - Esquive : TODO
   - Parade : TODO
   - Riposte : TODO
   - Contre : TODO
   - Contre-attaque : TODO
   - Désarmer : TODO
   - Combo : TODO
   - Toucher, Porter : TODO
- Implement the competences (ameliorations of the characteristics) :
   - Talents (manual activation before the fight; give a wound) : 0% done
   - Supers (randomly triggered during fight) : 0% done
   - Specialities (passive aptitudes) : 0% done
- Implement the pets : 0% done
- Implement the experience
   - XP gain after a fight : 0% done
   - XP levels : 0% done
   - Competences tree (specifications below) : 0% done
   - Change destiny after reaching a new XP level : 0% done
- Apply the functionalities to the profiles of the players
   - custom weapons list : TODO
   - custom skills list  : TODO
   - custom characteristics list : TODO
   - custom pets list : TODO
   - custom competences list : TODO


##  Specifications
### Competences tree
When reaching a new XP level, the player must chose 1 advantage among these 2 choices  :
- Choice 1: 
   - Upgrade a characteristic of +3 points
   - Upgrade a characteristic of +2 points, and another of +1 point
- Choice 2: a random element proposed among:
   - 1 weapon
   - 1 competence
   - 1 pet

Source : http://twin.tithom.fr/muxxu/labrute/experience/#arbre

## Resources
- French wiki : http://twin.tithom.fr/muxxu/labrute/
   - Liste des armes : http://twin.tithom.fr/muxxu/labrute/armes/
   - Liste des compétences : http://twin.tithom.fr/muxxu/labrute/competences/
   - Liste des caractéristiques (vie, endurance...) : http://twin.tithom.fr/muxxu/labrute/caracteristiques/
   - Liste des animaux : http://twin.tithom.fr/muxxu/labrute/familiers/
   - Expérience : http://twin.tithom.fr/muxxu/labrute/experience/
- English wiki : https://mybrutemuxxu.fandom.com/wiki/Mybrute_Wiki
   - List of weapons : https://mybrutemuxxu.fandom.com/wiki/Weapons
   - List of skills : https://mybrutemuxxu.fandom.com/wiki/Skills
   - List of characteristics (health, accuracy...) : https://mybrutemuxxu.fandom.com/wiki/Stats
   - List of pets : https://mybrutemuxxu.fandom.com/wiki/Pets
- Spanish wiki : https://elbrutomuxxu.fandom.com/wiki/
